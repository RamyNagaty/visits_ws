﻿using System;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Data.OracleClient;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Service : System.Web.Services.WebService
{
    OracleAcess oracleacess = new OracleAcess("CRM_ConnectionString");
    string SQL = "";
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //*********************************************************************** ( Users $ Password  ) ***************************
    #region " User Settings "

    [WebMethod]
    public bool Check_Loged_User (string User_Name , string Password)
    {
        bool ret_val = false ;

        try
        {
            string INC_PASS = Password.Insert(0, "W") ;
            string New_PASS = INC_PASS.Insert(4, "YM");
            SQL = "SELECT PASS FROM USERS WHERE USER_NAME = '"+ User_Name.ToUpper().ToString() +"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString().ToUpper().Trim() == New_PASS.ToUpper().Trim())
            {
                ret_val = true;
            }
            else
            {
                ret_val = false;
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Loged_User >> to check logged user and password");
            ret_val = false;
        }

        return ret_val;
    }

    [WebMethod]
    public bool Check_Lock_Users(string User_Name)
    {
        bool ret_val = false;

        try
        {
            SQL = "SELECT STATUS FROM USERS WHERE USER_NAME = '" + User_Name.ToUpper().ToString() + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString().ToUpper().Trim() == "Y")
            {
                ret_val = true;
            }
            else
            {
                ret_val = false;
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Loged_User >> to check logged user and password");
            ret_val = false;
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_User_Name(string User_Name)
    {
        string ret_val = "";

        try
        {
            SQL = " select arabic_name from users where user_name = '" + User_Name.ToUpper().ToString() + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_User_Name >> To get User Name");
        }

        return ret_val;
    }

    [WebMethod]
    public string Check_Authorization( string User_Name , string form_name )
    {
        string ret_val = "" ;

        try
        {   
            SQL = " select SV_priv('"+form_name+"' , '"+User_Name+"') from dual ";
            //SV_R001
            //SV_VISITS
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Authorization >> To check user privalages");
        }

        return ret_val ;
    }

    [WebMethod]
    public int Change_User_Password(string user, string pass)
    {
        int ret_val = 0;

        try
        {
            OracleParameter[] parameterList = new OracleParameter[]
                                                    {
                                                        new OracleParameter("USR",user ),
                                                        new OracleParameter("PW",pass),
                                                    };
            int RSLT = oracleacess.excute_PRC("CHANGE_PW", parameterList);
            ret_val = RSLT;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Change_User_Password >> Change User Password ");
        }

        return ret_val;
    }

    #endregion

    //*********************************************************************** ( Internal Func     ) *****************************
    #region " Internal Functions "

    public void LogError(string msg, string functionName)
    {
        StreamWriter sr = new StreamWriter(@"C:\Inetpub\wwwroot\SRV_CRM\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";

        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion
    //*********************************************************************** ( Customers         ) *****************************
    #region " save visit  "
    //
    [WebMethod]
    public DataSet Bind_EMP_DDL()
    {
        try
        {
            SQL = " select emp_no , emp_name from sv_emp order by emp_name ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_DDL >> Fill emp to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_EXP_TYPE_DDL()
    {
        try
        {
            SQL = " select EXP_TYPE from SV_EXP_TYPES order by EXP_TYPE ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EXP_TYPE_DDL >> Fill Expenses Types to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_DEPT_DDL()
    {
        try
        {
            SQL = " select Code , Name from SV_DEPT Order By Code ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_DEPT_DDL >> Fill Departments to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_EMP_By_DEPT_DDL(string Dept_code)
    {
        try
        {
            SQL = " select emp_no , emp_name from sv_emp Where Dept_code = '"+Dept_code+"' order by emp_name ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_By_DEPT_DDL >> Fill emp by department to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public string Get_Next_VISIT_No()
    {
        string ret_val = "";

        try
        {    
            SQL = "  select max(visit_no)+1 from sv_visits ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_VISIT_No >> To get Next Visit no ");
        }

        return ret_val;
    }


    [WebMethod]
    public string Get_Next_No()
    {
        string ret_val = "";

        try
        {
            SQL = "  select max(code)+1 from sv_visit_trans ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_no >> To get Next no ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_REP_CODE(string User_Name)
    {
        string ret_val = "";

        try
        {
            SQL = "  select code from Sv_Reps where rel_USER_NAME = '"+User_Name+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_REP_CODE >> To Rep Code ");
        }

        return ret_val;
    }

    //
    [WebMethod]
    public string Get_REP_MAIL(string user_name)
    {
        string ret_val = "";

        try
        {
            SQL = "  select email from Sv_Reps where REL_USER_NAME = '"+user_name+"' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = " ";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_REP_MAIL >> To get Next Visit no ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_EMP_MAIL( string emp_no)
    {
        string ret_val = "";

        try
        {
            SQL = "  select emp_mail from sv_emp where emp_no = '"+emp_no+"' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = " ";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_EMP_MAIL >> To get Next Visit no ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_EMP_CC_MAIL(string emp_no)
    {
        string ret_val = "";

        try
        {
            SQL = "  select cc_mail from sv_emp where emp_no = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = " ";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_EMP_CC_MAIL >> To get mail of cc ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_EMP_MANAGER_MAIL(string EMP_No)
    {
        string ret_val = "";

        try
        {
            SQL = " SELECT SV_MANG_MAIL('" + EMP_No + "') FROM DUAL ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = " ";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_EMP_MANAGER_MAIL >> To get rep manager Mail no ");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_REP_DDL()
    {
        try
        {
            SQL = " select CODE , code || '  ' || NAME as NAME FROM SV_REPS  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_REP_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Supervisor_DDL()
    {
        try
        {
            //where code in ( select mang_code from sv_reps )
            //select code , name from ( SELECT Code , Code || ' *** ' || Name as NAME FROM SV_REPS START WITH CODE = SV_USER_REP('H.HENEIDY') CONNECT BY PRIOR CODE = MANG_CODE ) where code <> SV_USER_REP('H.HENEIDY') 
            SQL = "  Select Code , Code || ' *** ' || Name as NAME from Sv_Reps order by name ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Supervisor_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Supervisors_DownLines_DDL(string User_Name)
    {
        try
        {
            //where code in ( select mang_code from sv_reps )
            //
            SQL = "  select code , name from ( SELECT Code , Code || ' *** ' || Name as NAME FROM SV_REPS START WITH CODE = SV_USER_REP('" + User_Name + "') CONNECT BY PRIOR CODE = MANG_CODE ) where code <> SV_USER_REP('" + User_Name + "')  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Supervisor_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL(string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS  Where RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' )  Order by SHOP_NAME ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Name(string Name, string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS WHERE   SHOP_NAME LIKE '%" + Name + "%'  AND RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' ) Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Name >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_CODE(string code, string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS WHERE   SAL_CODE = '" + code + "'  AND RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' ) Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Name >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Resp(string RESP, string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME   FROM SV_SHOPS WHERE  RESP_NAME LIKE '%" + RESP + "%'  AND RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' )  Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Resp >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Mobile(string Mobile, string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS WHERE  Mobile = '" + Mobile + "' AND RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' ) Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Mobile >> Fill Rep to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_SHOPS_DDL_By_Area(string Area, string user_name)
    {
        try
        {
            SQL = " select SAL_CODE AS CODE , SAL_CODE || ' *** ' || SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME   FROM SV_SHOPS WHERE  SUB_AREA LIKE '%" + Area + "%'  AND RECORD_TYPE IN ( SELECT RECORD_TYPE FROM USER_TRAD_TYPES WHERE USER_NAME = '" + user_name + "' ) Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_SHOPS_DDL_By_Area >> Fill Rep to DDL");
        }
        return Ds;
    }


    [WebMethod]
    public int Save_Visit_Data( string visit_no , string shop_code, string visit_date, string visit_time, string visit_details , string recommendations ,   string user_name  ,  string comp_notes , string buy_sorce , string gi_value , string gi_desc , string exp_type , string order_value  )
    {
        int val = 0;
        try
        {
            string rec_dte = DateTime.Now.ToString("dd/MM/yy");
            SQL = " INSERT INTO SV_VISITS ( visit_no , shop_code, visit_date, visit_time, visit_details, recommendations ,   user_name , rec_date  , COMP_NOTES , BUY_SOURCE , GI_VALUE , GI_DESC , EXP_TYPE , ORDER_VALUE ) ";
            SQL += " VALUES ( '" + visit_no + "','" + shop_code + "',  '" + visit_date + "' , '" + visit_time + "' , '" + visit_details + "', '" + recommendations + "' , '" + user_name + "' , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy")) + "','MM/DD/YYYY') ,  '" + comp_notes + "' , '" + buy_sorce + "' , '" + gi_value + "' , '" + gi_desc + "' , '" + exp_type + "' , '" + order_value + "'  ) ";

            //com.Parameters.AddWithValue("@file1", bytearr);
            //OracleParameter blobParameter = new OracleParameter();
            //blobParameter.OracleType = OracleType.LongRaw;
            //blobParameter.ParameterName = "IMAGE";
            //blobParameter.Value = compet_offers;
            //cmnd.Parameters.Add(blobParameter); 

            oracleacess.ExecuteNonQuery(SQL);
           // oracleacess.ExecuteNonQuery_PARM(SQL, blobParameter);
           
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Visit_Data >> save visits Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Save_Visit_Details(string visit_no, string emp_no , string msg_txt , string user  )
    {
        int val = 0;
        try
        {
            if (emp_no == "" || emp_no == "0" || emp_no == string.Empty || msg_txt == "")
            {
                val = 0;
            }
            else
            {
                string code = Get_Next_No();
                string rec_dte = DateTime.Now.ToString("dd/MM/yy");
                SQL = " INSERT INTO sv_visit_trans ( code , visit_no , message_text , user_name , rec_date , emp_no ) ";
                SQL += " VALUES ( '" + code + "' ,'" + visit_no + "' ,'" + msg_txt + "' ,'" + user + "' , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') ,'" + emp_no + "'   ) ";

                oracleacess.ExecuteNonQuery(SQL);
                val = 1;
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Visit_Details >> save visits Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Check_Saving_Data( string shop_code )
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(visit_no) as countt from sv_visits where shop_code = '" + shop_code + "' and to_char(rec_date ,'dd-MM-yyyy') =  to_char(sysdate ,'dd-MM-yyyy') ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saving_Data >> To get Next Visit no ");
        }

        return ret_val;
    }

    [WebMethod]
    public int Save_Visit_Compet(string visit_no, string compet_name , string compet_type , byte[] compet_img)
    {
        int val = 0;
        try
        {

            SQL = " INSERT INTO SV_COMPETITORS ( VISIT_ID , COMPET_NAME, COMPET_TYPE, COMPET_IMG  ) ";
            SQL += " VALUES ( '" + visit_no + "','" + compet_name + "', '" + compet_type + "' , :IMAGE ) ";

            //com.Parameters.AddWithValue("@file1", bytearr);
            OracleParameter blobParameter = new OracleParameter();
            blobParameter.OracleType = OracleType.LongRaw;
            blobParameter.ParameterName = "IMAGE";
            blobParameter.Value = compet_img;

            oracleacess.ExecuteNonQuery_PARM(SQL, blobParameter);

            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Visit_Compet >> save visits COMPETITORS ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Check_Redundancy(string shop_code , string User_name , string Day_Date)
    {
        int ret_val = 0;

        try
        {
            SQL = "  select Count(0) from sv_visits where shop_code = '"+shop_code+"' and User_Name = '"+User_name+"' and Visit_Date = '"+Day_Date+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Redundancy >> Check Visits Redundancy ");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_Govs()
    {
        try
        {
            SQL = " select code , name from Govs order by code ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Govs_DDL >> Fill govs DDl");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Dist(string city_code)
    {
        try
        {
            SQL = " select code , name from  district where city_code = '" + city_code + "' order by code ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Dist >> Fill dist DDl");
        }

        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Cities(string gov_code)
    {
        try
        {
            SQL = " select code , name from cities where gov_code = '" + gov_code + "' order by code ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Cities_DDL >> Fill cities DDl");
        }

        return Ds;
    }

    [WebMethod]
    public int Update_Shop_Master_Data(string shop_code, string gov, string city, string dist, string updated_by )
    {
        int val = 0;
        try
        {
            SQL = "  Update SV_SHOPS  set gov_id = '" + gov + "'  , city_id = '" + city + "'  , dist_id = '" + dist + "' , updated_by = '" + updated_by + "'  Where SAL_CODE = '" + shop_code + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Shop_Master_Data >> Make Approve");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Check_Govs_Data(string shop_code)
    {
        int  ret_val = 0;

        try
        {
            SQL = "  Select count(gov_id) from SV_SHOPS where sal_code = '" + shop_code + "' and gov_id is not null  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
             ret_val = int.Parse(Pass.ToString());
            
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Govs_Data >> To check on  ");
        }

        return ret_val;
    }

    #endregion

    //*********************************************************************** ( propa ganda survey screen )***************
    #region " Propaganda Survey  "

    [WebMethod]
    public string Get_Next_PS_No()
    {
        string ret_val = "";

        try
        {
            SQL = "  select max(id)+1 from sv_prop_survey ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_PS_No >> To get Next propaganda survey no ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Sub_Area_Name(string shop_code)
    {
        string ret_val = "";

        try
        {
            SQL = " select sub_area from sv_shops where sal_code = '" + shop_code + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Sub_Area_Name >> To get sub area ");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_PS_GRID()
    {
        try
        {
            SQL = "  select  a.ID ,a.SHOP_CODE ,b.shop_name , a.SUB_AREA , a.PROP_TYPE , a.PROP_NAME , a.STATUS , a.COMPET , a.PROP_COMP_STATUS , a.NOTES , a.REC_DATE , a.USER_NAME  from sv_prop_survey a , sv_shops b where a.shop_code = b.sal_code order by a.id desc ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_PS_GRID >> Fill PS Data to GridView");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_PS_Data(string shop_code, string sub_area, string prop_type , string prop_name , string status , string compet, string comp_status , string notes , string rec_date , string user_name )
    {
        int val = 0;
        try
        {
            rec_date = DateTime.Now.ToString("dd/MM/yy");
            string id = Get_Next_PS_No().ToString();
            SQL = " INSERT INTO SV_PROP_SURVEY ( ID , SHOP_CODE , SUB_AREA , PROP_TYPE , PROP_NAME , STATUS , COMPET , PROP_COMP_STATUS , NOTES , REC_DATE , USER_NAME  ) ";
            SQL += " VALUES (  '" + id + "' ,  '" + shop_code + "' ,  '" + sub_area + "' ,  '" + prop_type + "' ,  '" + prop_name + "' ,  '" + status + "' ,  '" + compet + "' , '" + comp_status + "' ,  '" + notes + "' ,  to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS')  ,  '" + user_name + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_PS_Data >> save PS Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Update_PS_Data( string ID , string shop_code, string sub_area, string prop_type, string prop_name, string status, string compet, string comp_status, string notes  )
    {
        int val = 0;
        try
        {

            SQL = " Update SV_PROP_SURVEY Set SHOP_CODE = '" + shop_code + "' , SUB_AREA = '" + sub_area +"' , PROP_TYPE = '" +prop_type +"' , PROP_NAME = '" +prop_name +"' , STATUS = '" +status +"' , COMPET = '" +compet +"' , PROP_COMP_STATUS = '" +comp_status +"' , NOTES = '" +notes +"' WHERE ID = '" + ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_PS_Data >> Update PS Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Delete_PS_Data( string ID)
    {
        int val = 0;
        try
        {

            SQL = " Delete From SV_PROP_SURVEY  WHERE ID  = '" + ID + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_PS_Data >> delete PS Data ");
            val = 0;
        }

        return val;
    }

    #endregion
    //*********************************************************************** ( Reps Data         ) *****************************
    #region " All Reports "

    [WebMethod]
    public DataSet Get_Visits_Rep(string from_date , string to_date , string user , string supervisor , string shop_code) 
    {
        try
        {
            SQL = " SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS, V.RECOMMENDATIONS , V.COMPETITORS , V.USER_NAME , V.REC_DATE , V.COMP_TYPE , V.COMP_NOTES ,  S.SHOP_NAME SHOP_NAME, R.NAME REP_NAME , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "'  AND R.CODE  = '" + supervisor + "' AND V.SHOP_CODE = '" + shop_code + "'  ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Without_Shops_Rep(string from_date, string to_date, string user, string supervisor )
    {
        try
        {
            SQL = " SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS, V.RECOMMENDATIONS , V.COMPETITORS , V.USER_NAME , V.REC_DATE , V.COMP_TYPE , V.COMP_NOTES ,  S.SHOP_NAME SHOP_NAME, R.NAME REP_NAME , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "'  AND R.CODE  = '" + supervisor + "'   ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Trans_Rep(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
            SQL = " SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME  , V.USER_NAME , V.REC_DATE , S.SHOP_NAME , R.NAME as REP_NAME , E.EMP_NAME , T.MESSAGE_TEXT , T.DEPT_REPLAY , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R , sv_visit_trans T , sv_emp E WHERE V.SHOP_CODE = S.SAL_CODE      AND V.REP_CODE = R.CODE       AND v.visit_no = t.visit_no      AND T.EMP_NO = E.EMP_NO      AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "'  AND R.CODE = '" + supervisor + "'  ORDER BY V.VISIT_NO   ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Trans_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Compet_Rep(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
            //SQL = "  SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME , C.COMPET_NAME AS COMPETITORS , V.USER_NAME , V.REC_DATE , C.COMPET_TYPE AS COMP_TYPE , V.COMP_NOTES ,  S.SHOP_NAME SHOP_NAME, R.NAME REP_NAME , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R , sv_competitors C WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_NO = C.VISIT_ID(+) AND V.VISIT_DATE BETWEEN  '" + from_date + "' AND '" + to_date + "' AND  C.COMPET_NAME <> '  '   AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO ";
            SQL = "  SELECT  V.VISIT_NO , V.VISIT_DATE , V.VISIT_TIME , C.COMPET_NAME AS COMPETITORS , V.USER_NAME , V.REC_DATE , C.COMPET_TYPE AS COMP_TYPE , V.COMP_NOTES ,  S.SHOP_NAME SHOP_NAME, R.NAME REP_NAME , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R , sv_competitors C WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_NO = C.VISIT_ID(+) AND V.VISIT_DATE BETWEEN  '" + from_date + "' AND '" + to_date + "' AND  C.COMPET_NAME <> '  '   AND R.CODE = '" + supervisor + "' ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Compet_Rep >> Fill Visites Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Approve_Rep(string from_date, string to_date, string user, string supervisor , string importance )
    {
        try
        {
            if (importance.Trim() != "0")
            {
                //SQL = " SELECT  V.VISIT_NO , R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND  V.APPROVED = 'Y' AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO , R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND  V.APPROVED = 'Y' AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "'  AND R.CODE = '" + supervisor + "'  ORDER BY V.VISIT_NO  ";
            }
            else
            {
                //SQL = " SELECT  V.VISIT_NO , R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND  V.APPROVED = 'Y'  AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO , R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND  V.APPROVED = 'Y'  AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND R.CODE  = '" + supervisor + "'  ORDER BY V.VISIT_NO  ";
            }
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Approve_Rep >> Fill Visits_Approve_Rep Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Not_Approve_Rep(string from_date, string to_date, string user, string supervisor , string importance )
    {
        try
        {
            if (importance.Trim() != "0")
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND  V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND  V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  R.CODE = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            }
            else
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  R.CODE = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            }
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Approve_Rep >> Fill Visits_Approve_Rep Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_All_Approve_Rep(string from_date, string to_date, string user, string supervisor, string importance )
    {
        try
        {
            if (importance.Trim() != "0")
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  R.CODE  = '" + supervisor + "'  ORDER BY V.VISIT_NO  ";
            }
            else
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE  AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND   R.CODE = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            }
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Approve_Rep >> Fill Visits_Approve_Rep Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Under_Approve_Rep(string from_date, string to_date, string user, string supervisor ,  string importance )
    {
        try
        {
            if (importance.Trim() != "0")
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.importance = '" + importance + "' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  R.CODE  = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            }
            else
            {
                //SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
                SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'اعتمد' when  V.APPROVED = 'N' then 'لم يعتمد' else 'تحت المراجعة' end as approve , S.AREA_NAME FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND R.CODE  = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            }
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Approve_Rep >> Fill Visits_Approve_Rep Report");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Visits_Target_Rep(string supervisor, string frm_month, string to_month, string year, string flag)
    {
        try
        {
            if ( flag.Trim() == "0" )
            {
                SQL = "  Select code , Name , region , branch , area_name , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','4') as target , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','1') as approved , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','2') as not_approved ,  SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','3') as under_rev from SV_REPS where code = '" + supervisor + "'  order by code  ";
            }
            else if (flag.Trim() == "1")
            {
                SQL = "  Select code , Name , region , branch , area_name , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','4') as target , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','1') as approved , SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','2') as not_approved ,  SV_VISITS_FN(code,'" + frm_month + "','" + to_month + "','" + year + "','3') as under_rev from SV_REPS order by code   ";
            }
                Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Visits_Target_Rep >> Fill Visites and Target Report");
        }
        return Ds;
    }

    #endregion

    //*******************************
    #region " Approve Page "

    [WebMethod]
    public DataSet Bind_Approved_Visits_GRID(string from_date, string to_date, string user, string supervisor)
    {
        try
        {

           // SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'Y' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
            SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'Y' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE = '" + supervisor + "' ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Visits_GRID >> ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Rejected_Visits_GRID(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
           // SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
            SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED = 'N' AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "'   AND  REP_CODE = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Visits_GRID >> ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Under_App_Visits_GRID(string from_date, string to_date, string user, string supervisor)
    {
        try
        {
           // SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
            SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND  V.APPROVED is null AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE = '" + supervisor + "' ORDER BY V.VISIT_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Visits_GRID >> ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Visits_GRID( string from_date, string to_date, string user, string supervisor )
    {
        try
        {
            //AND  V.APPROVED = 'Y' 
            //V.APPROVED = 'N' 
            //V.APPROVED is null
           // SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE IN( SELECT CODE FROM SV_REPS START WITH CODE = SV_USER_REP('" + user + "') AND R.CODE in ( select code from sv_reps where mang_code = '" + supervisor + "' or code = '" + supervisor + "') CONNECT BY PRIOR CODE = MANG_CODE ) ORDER BY V.VISIT_NO  ";
            SQL = " SELECT  V.VISIT_NO ,  R.NAME REP_NAME , S.SHOP_NAME SHOP_NAME , V.VISIT_DATE , V.VISIT_TIME , V.VISIT_DETAILS  , case when V.APPROVED = 'Y' then 'موافق' when  V.APPROVED = 'N' then 'غير موافق' else 'تحت المراجعة' end as approve  FROM SV_VISITS V,SV_SHOPS S,SV_REPS R WHERE V.SHOP_CODE = S.SAL_CODE AND V.REP_CODE = R.CODE AND V.VISIT_DATE BETWEEN '" + from_date + "' AND '" + to_date + "' AND  REP_CODE = '" + supervisor + "' ORDER BY V.VISIT_NO  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Visits_GRID >> ");
        }
        return Ds;
    }


    [WebMethod]
    public int Visit_Approve(string Visit_no ,  string Reason , string imp )
    {
        int val = 0;
        try
        {
            SQL = " UPDATE sv_visits Set APPROVED = 'Y' , REJECT_REASON = '" + Reason + "' , importance = '"+imp+"' where VISIT_NO = '" + Visit_no + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Visit_Approve >> Make Approve");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Visit_Reject(string Visit_no, string REJECT_REASON  , string imp )
    {
        int val = 0;
        try
        {
            SQL = " UPDATE sv_visits Set APPROVED = 'N' , REJECT_REASON = '" + REJECT_REASON + "' , importance = '" + imp + "' where VISIT_NO = '" + Visit_no + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Visit_Reject >> Make Approve");
            val = 0;
        }

        return val;
    }

    #endregion

    //*******************************
    #region " Reps Targets "

    [WebMethod]
    public DataSet Bind_Targets_GRID()
    {
        try
        {
            SQL = " select A.REP_CODE , B.NAME , A.MONTH_NO , A.YEAR_NO , A.TARGET from SV_REP_TARGET A , sv_reps B  where A.REP_CODE = B.CODE order by  A.MONTH_NO , A.YEAR_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Targets_GRID >> ");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_Target_Data(string rep_code, string month , string year , string target )
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO  SV_REP_TARGET (  rep_code , month_no , year_no , TARGET ) ";
            SQL += " VALUES (  '" + rep_code + "' ,  '" + month + "' ,  '" + year + "' ,  '" + target + "'  ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Target_Data >> save target Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Update_Target_Data(string rep_code, string month, string year, string target)
    {
        int val = 0;
        try
        {

            SQL = " Update SV_REP_TARGET Set  TARGET = '" + target + "' WHERE rep_code = '" + rep_code + "' AND month_no = '" + month + "' AND year_no = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Target_Data >> Update target Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Delete_Target_Data(string rep_code, string month, string year, string target)
    {
        int val = 0;
        try
        {

            SQL = " Delete From SV_REP_TARGET  WHERE rep_code = '" + rep_code + "' AND month_no = '" + month + "' AND year_no = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Target_Data >> delete target Data ");
            val = 0;
        }

        return val;
    }

    #endregion

    //*********************************************************************** ( Android Functions Only ) *****************************
    #region " Android Methods Only  "

    public struct ShopsData
    {
        public string code;
        public string name;
    }

    public struct ShopsData_GPS
    {
        public string code;
        public string name;
        public string lat;
        public string lang;
    }

    public struct REP_PLAN
    {
        public string shop_code;
        public string shop_name;
        public string v_date;
        
    }

    [WebMethod]
    public ShopsData[] GetShopsList()
    {
        String code;
        String Name;

        ShopsData[] names = null;

        try
        {
            SQL = " select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME  FROM SV_SHOPS Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
            int no = dt.Rows.Count;

            names = new ShopsData[no];

            for (int i = 0; i < no; i++)
            {
                code = dt.Rows[i]["CODE"].ToString();
                Name = dt.Rows[i]["NAME"].ToString();
               

                names[i].code = code;
                names[i].name = Name;
                
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Array --->> Fill Array Data");
        }
        return names;
    }

    [WebMethod]
    public ShopsData_GPS[] GetShopsList_by_Lang()
    {
        String code;
        String Name;
        String LAT;
        String LANG;

        ShopsData_GPS[] names = null;

        try
        {
            //SQL = " Select SAL_CODE AS CODE , SHOP_NAME || ' *** ' || SUB_AREA || ' *** ' || RESP_NAME   AS NAME   FROM SV_SHOPS Where ( to_number(latitude) BETWEEN " + lat + " - (1.0 / 111.045)             AND " + lat + " + (1.0 / 111.045) )          AND          ( to_number(longtude) BETWEEN " + lang + " - (1.0 / 111.045)             AND " + lang + " + (1.0 / 111.045)           )  Order by SHOP_NAME  ";
            SQL = " Select SAL_CODE AS CODE , SHOP_NAME  AS NAME , latitude , longtude  FROM SV_SHOPS   Order by SHOP_NAME  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
            int no = dt.Rows.Count;

            names = new ShopsData_GPS[no];

            for (int i = 0; i < no; i++)
            {
                code = dt.Rows[i]["CODE"].ToString();
                Name = dt.Rows[i]["NAME"].ToString();
                LAT = dt.Rows[i]["latitude"].ToString();
                LANG = dt.Rows[i]["longtude"].ToString();

                names[i].code = code;
                names[i].name = Name;
                names[i].lat = LAT;
                names[i].lang = LANG;

            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Array --->> Fill Array Data");
        }
        return names;
    }

    [WebMethod]
    public REP_PLAN[] Get_Rep_Plans( string user)
    {
        String shop_code;
        String shop_name;
        String v_date;

        REP_PLAN[] names = null;

        try
        {

            SQL = " select s.shop_name||' - '||s.trad_name||' - '||s.gov_name shop_name,p.shop_code , trunc(sysdate) as V_Date from sv_shops s,sv_rep_plan p where p.shop_code = s.sal_code and p.visit_date = trunc(sysdate) and p.rep_code in(select code from sv_reps where rel_user_name = '" + user + "') ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
            int no = dt.Rows.Count;

            names = new REP_PLAN[no];

            for (int i = 0; i < no; i++)
            {
                shop_name = dt.Rows[i]["SHOP_NAME"].ToString();
                shop_code = dt.Rows[i]["SHOP_CODE"].ToString();
                v_date = dt.Rows[i]["V_DATE"].ToString();

                names[i].shop_code = shop_code;
                names[i].shop_name = shop_name;
                names[i].v_date = v_date;

            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Array --->> Fill Array Data");
        }
        return names;
    }

    [WebMethod]
    public string Get_Next_Android_No()
    {
        string ret_val = "";

        try
        {
            SQL = " select max(visit_no)+1 from sv_android ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "" || Pass == null ) 
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_Android_No >> To get Next no ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Save_Visits_android(string shop_code, string visit_date, string visit_time, string visit_details, string recommendations, string competitors, string user_name, string comp_type, string comp_notes, string latitude, string longitude, string gps_addres, byte[] image )
    {
        string val = "0";
        try
        {
            string code = Get_Next_Android_No();
            string rec_dte = DateTime.Now.ToString("dd/MM/yy");
            SQL = " INSERT INTO SV_ANDROID ( visit_no , shop_code, visit_date, visit_time, visit_details, recommendations, competitors ,  user_name , rec_date ,  COMP_TYPE , COMP_NOTES , LATITUDE , LONGITUDE , GPS_ADDRESS , IMAGE ) ";
            SQL += " VALUES ( '" + code + "','" + shop_code + "',  to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') , '" + visit_time + "' , '" + visit_details + "', '" + recommendations + "' , '" + competitors + "',  '" + user_name + "' , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') ,  '" + comp_type + "' , '" + comp_notes + "','"+latitude+"','"+longitude+"','"+gps_addres+"', :IMAGE  ) ";

            OracleParameter blobParameter = new OracleParameter();
            blobParameter.OracleType = OracleType.LongRaw;
            blobParameter.ParameterName = "IMAGE";
            blobParameter.Value = image ;

            oracleacess.ExecuteNonQuery_PARM(SQL, blobParameter);

           // oracleacess.ExecuteNonQuery(SQL);
            val = "1" ;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Visits_android >> save visits Data ");
            val = "0" ;
        }

        return val;
    }

    #endregion

}